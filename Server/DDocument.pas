unit DDocument;

interface

uses
  System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.Comp.Client, Data.DB;

type
  TOnCompressNextFile = procedure(var StopProcessing: Boolean) of object;

  TdmoDocument = class(TDataModule)
    conDownloadDB: TFDConnection;
    cmdUpdZIPFile: TFDCommand;
  private
    { Private declarations }
    FOnCompressNextFile: TOnCompressNextFile;
    procedure AbZipperArchiveSaveProgress(Sender: TObject;
                                          Progress: Byte;
                                          var Abort: Boolean);
    procedure SetOnCompressNextFile(const Value: TOnCompressNextFile);
    procedure MarkZIPFileAsCompleted(const ZIPFileID: Integer);
  public
    { Public declarations }
    procedure CompressFiles(const ZIPFileID: Integer);
    property OnCompressNextFile: TOnCompressNextFile read FOnCompressNextFile write SetOnCompressNextFile;
  end;

implementation

uses
  System.SysUtils,
  System.Types,
  System.IOUtils,
  AbZipper;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoDocument }

procedure TdmoDocument.AbZipperArchiveSaveProgress(Sender: TObject;
                                                   Progress: Byte;
                                                   var Abort: Boolean);
var
  StopProcessing: Boolean;
begin
  StopProcessing := False;
  if Assigned(FOnCompressNextFile) then
    FOnCompressNextFile(StopProcessing);
  Abort := StopProcessing;
end;

procedure TdmoDocument.CompressFiles(const ZIPFileID: Integer);
var
  FileNames: TStringDynArray;
  FileName, FolderName: string;
  ZipFile: TAbZipper;
  StartPos: Integer;
  StopProcessing: Boolean;
begin
  StopProcessing := False;
  ZIPFile := TAbZipper.Create(Self);
  try
    ZipFile.FileName := IncludeTrailingPathDelimiter(TPath.GetTempPath) + ZIPFileID.ToString + '.zip';
    //ZipFile.Password := 'YourPassword';
    ZipFile.OnArchiveSaveProgress := AbZipperArchiveSaveProgress;
    try
      FolderName := ExpandFileName(ExtractFilePath(ParamStr(0)) + '..\..\..\Documents');
      ZIPFile.BaseDirectory := FolderName;
      StartPos := IncludeTrailingPathDelimiter(FolderName).Length+1;
      FileNames := TDirectory.GetFiles(FolderName, '*.*', TSearchOption.soAllDirectories);
      for FileName in FileNames do
      begin
        if Assigned(FOnCompressNextFile) then
          FOnCompressNextFile(StopProcessing);
        if StopProcessing then
          Exit;
        ZipFile.AddFiles(Copy(FileName, StartPos, Length(FileName)+1), faAnyFile);
      end;
    finally
      ZipFile.Save;
      MarkZIPFileAsCompleted(ZIPFileID);
    end;
  finally
    ZipFile.Free;
  end;
end;

procedure TdmoDocument.MarkZIPFileAsCompleted(const ZIPFileID: Integer);
begin
  conDownloadDB.Open;
  try
    cmdUpdZIPFile.ParamByName('ZIPFileID').AsInteger := ZIPFileID;
    cmdUpdZIPFile.Execute;
  finally
    conDownloadDB.Close;
  end;
end;

procedure TdmoDocument.SetOnCompressNextFile(const Value: TOnCompressNextFile);
begin
  FOnCompressNextFile := Value;
end;

end.
