unit SDownloadMethods;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.DSAuth,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
{$METHODINFO ON}
  TsvmDownloadMethods = class(TDataModule)
    conDownloadDB: TFDConnection;
    qryGetPK: TFDQuery;
    cmdInsZIPFile: TFDCommand;
    cmdMarkAsDeleted: TFDCommand;
    qryIsZIPFileCompleted: TFDQuery;
    cmdUpdZIPFile: TFDCommand;
  private
    { Private declarations }
    function GetNextPK: Integer;
    function CreateZIPFileRecord: Integer;
    procedure MarkZIPFileAsDownloaded(const ZIPFileID: Integer);
    procedure MarkZIPFileAsDeleted(const ZIPFileID: Integer);
  public
    { Public declarations }
    procedure CompressFiles(out ZIPFileID: Integer);
    function ZIPFileHasBeenCreatedCompletely(const ZIPFileID: Integer): Boolean;
    procedure FetchZIPFile(const ZIPFileID: Integer;
                           const Position: Int64;
                           out Buffer: TStream;
                           out BufferSize: Int64);
  end;
{$METHODINFO OFF}

implementation


{$R *.dfm}


uses
  System.SysUtils,
  System.IOUtils,
  System.Math,
  UCompressWorkerThreadList,
  UCompressWorkerThread,
  UCommon;

{ TDownloadMethods }

procedure TsvmDownloadMethods.CompressFiles(out ZIPFileID: Integer);
begin
  ZIPFileID := CreateZIPFileRecord;
  CompressWorkerThreadList.AddCompressWorkerThread(TCompressWorkerThread.Create(ZIPFileID));
end;

function TsvmDownloadMethods.CreateZIPFileRecord: Integer;
begin
  Result := GetNextPK;
  cmdInsZIPFile.ParamByName('ZIPFileID').AsInteger := Result;
  cmdInsZIPFile.Execute;
end;

procedure TsvmDownloadMethods.FetchZIPFile(const ZIPFileID: Integer;
                                           const Position: Int64;
                                           out Buffer: TStream;
                                           out BufferSize: Int64);
var
  FileName: string;
  fsZIPFile: TFileStream;
  DownloadCompleted: Boolean;
begin
  Buffer := TMemoryStream.Create;
  BufferSize := 0;
  DownloadCompleted := False;
  FileName := IncludeTrailingPathDelimiter(TPath.GetTempPath) + ZIPFileID.ToString + '.zip';
  if FileExists(FileName) then
  begin
    fsZIPFile := TFileStream.Create(FileName, fmOpenRead);
    try
      fsZIPFile.Position := Position;
      BufferSize := Min(cZIPFileChunk, fsZIPFile.Size-Position);
      Buffer.CopyFrom(fsZIPFile, BufferSize);
      Buffer.Position := 0;
      if BufferSize <= cZIPFileChunk then // we have read to the end of the ZIP file
      begin
        MarkZIPFileAsDownloaded(ZIPFileID);
        DownloadCompleted := True;
      end;
    finally
      fsZIPFile.Free;
    end;
    if DownloadCompleted then
    begin
      MarkZIPFileAsDeleted(ZIPFileID);
      DeleteFile(FileName);
    end;
  end;
end;

function TsvmDownloadMethods.GetNextPK: Integer;
begin
  qryGetPK.Open;
  try
    Result := qryGetPK.Fields[0].AsInteger;
  finally
    qryGetPK.Close;
  end;
end;

procedure TsvmDownloadMethods.MarkZIPFileAsDeleted(const ZIPFileID: Integer);
begin
  cmdMarkAsDeleted.ParamByName('ZIPFileID').AsInteger := ZIPFileID;
  cmdMarkAsDeleted.Execute;
end;

procedure TsvmDownloadMethods.MarkZIPFileAsDownloaded(const ZIPFileID: Integer);
begin
  cmdUpdZIPFile.ParamByName('ZIPFileID').AsInteger := ZIPFileID;
  cmdUpdZIPFile.Execute;
end;

function TsvmDownloadMethods.ZIPFileHasBeenCreatedCompletely(const ZIPFileID: Integer): Boolean;
begin
  qryIsZIPFileCompleted.ParamByName('ZIPFileID').AsInteger := ZIPFileID;
  qryIsZIPFileCompleted.Open;
  try
    Result := not qryIsZIPFileCompleted.Fields[0].IsNull;
  finally
    qryIsZIPFileCompleted.Close;
  end;
end;

end.

