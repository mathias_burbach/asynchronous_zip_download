program DownloadServer;

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FMain in 'FMain.pas' {frmMain},
  SDownloadMethods in 'SDownloadMethods.pas' {svmDownloadMethods: TDataModule},
  SDownloadServer in 'SDownloadServer.pas' {svcDownloadServer: TDataModule},
  UCompressWorkerThread in 'UCompressWorkerThread.pas',
  DDocument in 'DDocument.pas' {dmoDocument: TDataModule},
  UCompressWorkerThreadList in 'UCompressWorkerThreadList.pas',
  UCommon in '..\Common\UCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TsvcDownloadServer, svcDownloadServer);
  Application.Run;
end.

