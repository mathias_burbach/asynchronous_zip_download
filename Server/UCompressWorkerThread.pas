unit UCompressWorkerThread;

interface

uses
  System.Classes,
  Datasnap.DSServer;

type
  TCompressWorkerThread = class(TThread)
  private
    FZIPFileID: Integer;
    procedure DoBeforeCompressNextFile(var StopProcessing: Boolean);
  public
    constructor Create(const ZIPFileID: Integer); overload;
    procedure Execute; override;
    property ZIPFileID: Integer read FZIPFileID;
  end;

implementation

uses
  DDocument;

{ TCompressWorkerThread }

constructor TCompressWorkerThread.Create(const ZIPFileID: Integer);
begin
  FZIPFileID := ZIPFileID;
  FreeOnTerminate := True;
  inherited Create(True);
end;

procedure TCompressWorkerThread.DoBeforeCompressNextFile(var StopProcessing: Boolean);
begin
  StopProcessing := Terminated;
end;

procedure TCompressWorkerThread.Execute;
var
  dmoDocument: TdmoDocument;
begin
  try
    dmoDocument := TdmoDocument.Create(nil);
    try
      dmoDocument.OnCompressNextFile := DoBeforeCompressNextFile;
      dmoDocument.CompressFiles(FZIPFileID);
    finally
      dmoDocument.Free;
    end;
  except
    // log problem here but let thread finish gracefully
  end;
end;

end.
