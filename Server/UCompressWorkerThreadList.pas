unit UCompressWorkerThreadList;

interface

uses
  System.Classes,
  UCompressWorkerThread;

type
  TCompressWorkerThreadList = class
  private
    FThreadList: TStringList;
    procedure TerminateAllRunningThreads;
    procedure DoOnTerminate(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddCompressWorkerThread(const CompressWorkerThread: TCompressWorkerThread);
  end;

function CompressWorkerThreadList: TCompressWorkerThreadList;

implementation

uses
  System.SysUtils;

var
  InternalCompressWorkerThreadList: TCompressWorkerThreadList;

function CompressWorkerThreadList: TCompressWorkerThreadList;
begin
  Result := InternalCompressWorkerThreadList;
end;

{ TCompressWorkerThreadList }

procedure TCompressWorkerThreadList.AddCompressWorkerThread(const CompressWorkerThread: TCompressWorkerThread);
begin
  TMonitor.Enter(FThreadList);
  try
    CompressWorkerThread.OnTerminate := DoOnTerminate;
    FThreadList.AddObject(IntToStr(CompressWorkerThread.ZIPFileID), CompressWorkerThread);
    CompressWorkerThread.Start;
  finally
    TMonitor.Exit(FThreadList);
  end;
end;

constructor TCompressWorkerThreadList.Create;
begin
  FThreadList := TStringList.Create;
end;

destructor TCompressWorkerThreadList.Destroy;
begin
  TerminateAllRunningThreads;
  FThreadList.Free;
  inherited;
end;

procedure TCompressWorkerThreadList.DoOnTerminate(Sender: TObject);
var
  CompressWorkerThread: TCompressWorkerThread;
  Index: Integer;
begin
  if Sender is TCompressWorkerThread then
  begin
    CompressWorkerThread := Sender as TCompressWorkerThread;
    TMonitor.Enter(FThreadList);
    try
      if FThreadList.Find(IntToStr(CompressWorkerThread.ZIPFileID), Index) then
        FThreadList.Delete(Index); // TCompressWorkerThread will destroy itself
    finally
      TMonitor.Exit(FThreadList);
    end;
  end;
end;

procedure TCompressWorkerThreadList.TerminateAllRunningThreads;
var
  CompressWorkerThread: TCompressWorkerThread;
  Index: Integer;
begin
  TMonitor.Enter(FThreadList);
  try
    for Index := 0 to FThreadList.Count-1 do
    begin
      CompressWorkerThread := FThreadList.Objects[Index] as TCompressWorkerThread;
      CompressWorkerThread.Terminate;
    end;
  finally
    TMonitor.Exit(FThreadList);
  end;
end;


initialization
  InternalCompressWorkerThreadList := TCompressWorkerThreadList.Create;

finalization
  //InternalCompressWorkerThreadList.Free;
  InternalCompressWorkerThreadList.Destroy;

end.
