unit SDownloadServer;

interface

uses
  System.Classes,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSServer,
  Datasnap.DSCommonServer,
  Datasnap.DSAuth,
  IPPeerServer,
  IPPeerAPI;

type
  TsvcDownloadServer = class(TDataModule)
    dsServer: TDSServer;
    dsTCPTransport: TDSTCPServerTransport;
    sclDownloadMethods: TDSServerClass;
    procedure sclDownloadMethodsGetClass(DSServerClass: TDSServerClass;
                                         var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
  end;

var
  svcDownloadServer: TsvcDownloadServer;

implementation


{$R *.dfm}

uses
  SDownloadMethods;

procedure TsvcDownloadServer.sclDownloadMethodsGetClass(DSServerClass: TDSServerClass;
                                                        var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmDownloadMethods;
end;

end.

