object svcDownloadServer: TsvcDownloadServer
  OldCreateOrder = False
  Height = 271
  Width = 415
  object dsServer: TDSServer
    Left = 96
    Top = 11
  end
  object dsTCPTransport: TDSTCPServerTransport
    Server = dsServer
    Filters = <>
    Left = 96
    Top = 73
  end
  object sclDownloadMethods: TDSServerClass
    OnGetClass = sclDownloadMethodsGetClass
    Server = dsServer
    LifeCycle = 'Invocation'
    Left = 200
    Top = 11
  end
end
