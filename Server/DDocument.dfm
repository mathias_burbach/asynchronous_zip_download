object dmoDocument: TdmoDocument
  OldCreateOrder = False
  Height = 150
  Width = 329
  object conDownloadDB: TFDConnection
    Params.Strings = (
      'Database=DownloadDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Protocol=TCPIP'
      'Server=127.0.0.1'
      'DriverID=FB')
    FetchOptions.AssignedValues = [evUnidirectional]
    FetchOptions.Unidirectional = True
    LoginPrompt = False
    Left = 48
    Top = 40
  end
  object cmdUpdZIPFile: TFDCommand
    Connection = conDownloadDB
    CommandKind = skUpdate
    CommandText.Strings = (
      'Update ZIPFile'
      'Set CompletedStmp = Current_Timestamp'
      'Where ZIPFileID = :ZIPFileID')
    ParamData = <
      item
        Name = 'ZIPFILEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    Left = 151
    Top = 40
  end
end
