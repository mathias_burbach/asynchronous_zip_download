object svmDownloadMethods: TsvmDownloadMethods
  OldCreateOrder = False
  Height = 177
  Width = 575
  object conDownloadDB: TFDConnection
    Params.Strings = (
      'Database=DownloadDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Protocol=TCPIP'
      'Server=127.0.0.1'
      'DriverID=FB')
    FetchOptions.AssignedValues = [evUnidirectional]
    FetchOptions.Unidirectional = True
    LoginPrompt = False
    Left = 48
    Top = 40
  end
  object qryGetPK: TFDQuery
    Connection = conDownloadDB
    SQL.Strings = (
      'Select Next Value For DownloadDBSeq From RDB$Database')
    Left = 144
    Top = 40
  end
  object cmdInsZIPFile: TFDCommand
    Connection = conDownloadDB
    CommandKind = skInsert
    CommandText.Strings = (
      'Insert Into ZIPFile'
      '(ZIPFileID, CreatedStmp)'
      'Values'
      '(:ZIPFileID, Current_Timestamp)')
    ParamData = <
      item
        Name = 'ZIPFILEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    Left = 232
    Top = 40
  end
  object cmdMarkAsDeleted: TFDCommand
    Connection = conDownloadDB
    CommandKind = skUpdate
    CommandText.Strings = (
      'Update ZIPFile'
      'Set DeletedStmp = Current_Timestamp'
      'Where ZIPFileID = :ZIPFileID')
    ParamData = <
      item
        Name = 'ZIPFILEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    Left = 432
    Top = 41
  end
  object qryIsZIPFileCompleted: TFDQuery
    Connection = conDownloadDB
    SQL.Strings = (
      'Select CompletedStmp'
      'From ZIPFile'
      'Where ZIPFileID = :ZIPFileID')
    Left = 160
    Top = 104
    ParamData = <
      item
        Name = 'ZIPFILEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object cmdUpdZIPFile: TFDCommand
    Connection = conDownloadDB
    CommandKind = skUpdate
    CommandText.Strings = (
      'Update ZIPFile'
      'Set DownloadedStmp = Current_Timestamp'
      'Where ZIPFileID = :ZIPFileID')
    ParamData = <
      item
        Name = 'ZIPFILEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    Left = 327
    Top = 40
  end
end
