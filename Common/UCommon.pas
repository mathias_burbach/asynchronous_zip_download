unit UCommon;

interface

const
  cZIPFileChunk = 10 * 1024 * 1024; // 10 MB chunks

function GetFileSize(const FileName: string): Longint;

implementation

uses
  Winapi.Windows;

function GetFileSize(const FileName: string): Longint;
var
  info: TWin32FileAttributeData;
begin
  Result := -1;
  if GetFileAttributesEx(PWideChar(FileName), GetFileExInfoStandard, @info) then
    Result := Int64(info.nFileSizeLow) or Int64(info.nFileSizeHigh shl 32);
end;

end.
