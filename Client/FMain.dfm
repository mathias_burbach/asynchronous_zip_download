object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Download Client'
  ClientHeight = 179
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 19
  object lblZIPFileName: TLabel
    Left = 24
    Top = 48
    Width = 99
    Height = 19
    Caption = 'ZIP File Name'
    FocusControl = btnZIPFileName
  end
  object edtZIPFileName: TEdit
    Left = 24
    Top = 73
    Width = 409
    Height = 27
    TabOrder = 0
    OnChange = edtZIPFileNameChange
  end
  object btnZIPFileName: TButton
    Left = 439
    Top = 74
    Width = 27
    Height = 27
    Caption = '...'
    TabOrder = 1
    OnClick = btnZIPFileNameClick
  end
  object btnDownload: TButton
    Left = 128
    Top = 128
    Width = 217
    Height = 25
    Caption = 'Download ZIP Archive'
    Enabled = False
    TabOrder = 2
    OnClick = btnDownloadClick
  end
  object dlgSave: TSaveDialog
    Filter = 'ZIP Archive|*.zip'
    Left = 344
    Top = 16
  end
end
