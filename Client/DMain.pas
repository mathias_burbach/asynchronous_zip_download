unit DMain;

interface

uses
  System.Classes,
  Data.DB,
  Data.SqlExpr,
  Data.DbxDatasnap,
  Data.DBXCommon,
  IPPeerClient, Vcl.ExtCtrls;

type
  TdmoMain = class(TDataModule)
    conDownloadServer: TSQLConnection;
    timDownload: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure timDownloadTimer(Sender: TObject);
  private
    { Private declarations }
    FZIPFileName: string;
    FZIPFileID: Integer;
    FOnDownloadCompleted: TNotifyEvent;
    function ZIPFileHasBeenCreatedCompletely: Boolean;
    procedure FetchZIPFile;
    procedure SetOnDownloadCompleted(const Value: TNotifyEvent);
  public
    { Public declarations }
    procedure DownloadCompressedFiles(const ZIPFileName: string);
    property OnDownloadCompleted: TNotifyEvent read FOnDownloadCompleted write SetOnDownloadCompleted;
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  UServerProxy,
  UCommon;

{$R *.dfm}

{ TdmoMain }

procedure TdmoMain.DataModuleCreate(Sender: TObject);
begin
  conDownloadServer.Open;
end;

procedure TdmoMain.DataModuleDestroy(Sender: TObject);
begin
  conDownloadServer.Close;
end;

procedure TdmoMain.DownloadCompressedFiles(const ZIPFileName: string);
var
  svmDownloadMethodsClient: TsvmDownloadMethodsClient;
begin
  FZIPFileName := ZIPFileName;
  svmDownloadMethodsClient := TsvmDownloadMethodsClient.Create(conDownloadServer.DBXConnection);
  try
    svmDownloadMethodsClient.CompressFiles(FZIPFileID);
    timDownload.Enabled := True;
  finally
    svmDownloadMethodsClient.Free;
  end;
end;

procedure TdmoMain.FetchZIPFile;
var
  svmDownloadMethodsClient: TsvmDownloadMethodsClient;
  fsFetchedZIPFile: TFileStream;
  Position: Int64;
  Buffer: TStream;
  BufferSize: Int64;
begin
  svmDownloadMethodsClient := TsvmDownloadMethodsClient.Create(conDownloadServer.DBXConnection);
  try
    fsFetchedZIPFile := TFileStream.Create(FZIPFileName, fmCreate);
    try
      Position := 0;
      repeat
        svmDownloadMethodsClient.FetchZIPFile(FZIPFileID, Position, Buffer, BufferSize);
        Buffer.Position := 0;
        fsFetchedZIPFile.CopyFrom(Buffer, BufferSize);
        Position := Position + BufferSize;
      until (BufferSize < cZIPFileChunk);
      if Assigned(FOnDownloadCompleted) then
        FOnDownloadCompleted(Self);
    finally
      fsFetchedZIPFile.Free;
    end;
  finally
    svmDownloadMethodsClient.Free;
  end;
end;

procedure TdmoMain.SetOnDownloadCompleted(const Value: TNotifyEvent);
begin
  FOnDownloadCompleted := Value;
end;

procedure TdmoMain.timDownloadTimer(Sender: TObject);
begin
  timDownload.Enabled := False;
  if ZIPFileHasBeenCreatedCompletely then
    FetchZIPFile
  else
    timDownload.Enabled := True;
end;

function TdmoMain.ZIPFileHasBeenCreatedCompletely: Boolean;
var
  svmDownloadMethodsClient: TsvmDownloadMethodsClient;
begin
  svmDownloadMethodsClient := TsvmDownloadMethodsClient.Create(conDownloadServer.DBXConnection);
  try
    Result := svmDownloadMethodsClient.ZIPFileHasBeenCreatedCompletely(FZIPFileID);
  finally
    svmDownloadMethodsClient.Free;
  end;
end;

end.
