object dmoMain: TdmoMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 150
  Width = 309
  object conDownloadServer: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DbxDatasnap'
      'HostName=localhost'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/')
    Left = 64
    Top = 40
    UniqueId = '{B432F9BE-9FDB-44BD-92D3-E9EDA3DF74BB}'
  end
  object timDownload: TTimer
    Enabled = False
    OnTimer = timDownloadTimer
    Left = 176
    Top = 40
  end
end
