program DownloadClient;

uses
  Vcl.Forms,
  FMain in 'FMain.pas' {frmMain},
  DMain in 'DMain.pas' {dmoMain: TDataModule},
  UServerProxy in 'UServerProxy.pas',
  UCommon in '..\Common\UCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmoMain, dmoMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
