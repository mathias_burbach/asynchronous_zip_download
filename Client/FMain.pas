unit FMain;

interface

uses
  System.Classes,
  Vcl.Controls,
  Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TfrmMain = class(TForm)
    lblZIPFileName: TLabel;
    edtZIPFileName: TEdit;
    btnZIPFileName: TButton;
    dlgSave: TSaveDialog;
    btnDownload: TButton;
    procedure btnZIPFileNameClick(Sender: TObject);
    procedure edtZIPFileNameChange(Sender: TObject);
    procedure btnDownloadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure DoDownloadCompleted(Sender: TObject);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  System.SysUtils, DMain;

{$R *.dfm}

procedure TfrmMain.btnDownloadClick(Sender: TObject);
begin
  dmoMain.DownloadCompressedFiles(edtZIPFileName.Text);
  btnDownload.Enabled := False;
end;

procedure TfrmMain.btnZIPFileNameClick(Sender: TObject);
begin
  if dlgSave.Execute then
    edtZIPFileName.Text := dlgSave.FileName;
end;

procedure TfrmMain.DoDownloadCompleted(Sender: TObject);
begin
  btnDownload.Enabled := True;
end;

procedure TfrmMain.edtZIPFileNameChange(Sender: TObject);
begin
  btnDownload.Enabled := (edtZIPFileName.Text <> EmptyStr);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dmoMain.OnDownloadCompleted := DoDownloadCompleted;
end;

end.
