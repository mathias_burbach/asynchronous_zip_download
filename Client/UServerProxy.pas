// 
// Created by the DataSnap proxy generator.
// 17/11/2020 4:59:45 PM
// 

unit UServerProxy;

interface

uses System.JSON, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.DBXJSONReflect;

type
  TsvmDownloadMethodsClient = class(TDSAdminClient)
  private
    FCompressFilesCommand: TDBXCommand;
    FZIPFileHasBeenCreatedCompletelyCommand: TDBXCommand;
    FFetchZIPFileCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure CompressFiles(out ZIPFileID: Integer);
    function ZIPFileHasBeenCreatedCompletely(ZIPFileID: Integer): Boolean;
    procedure FetchZIPFile(ZIPFileID: Integer; Position: Int64; out Buffer: TStream; out BufferSize: Int64);
  end;

implementation

procedure TsvmDownloadMethodsClient.CompressFiles(out ZIPFileID: Integer);
begin
  if FCompressFilesCommand = nil then
  begin
    FCompressFilesCommand := FDBXConnection.CreateCommand;
    FCompressFilesCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCompressFilesCommand.Text := 'TsvmDownloadMethods.CompressFiles';
    FCompressFilesCommand.Prepare;
  end;
  FCompressFilesCommand.ExecuteUpdate;
  ZIPFileID := FCompressFilesCommand.Parameters[0].Value.GetInt32;
end;

function TsvmDownloadMethodsClient.ZIPFileHasBeenCreatedCompletely(ZIPFileID: Integer): Boolean;
begin
  if FZIPFileHasBeenCreatedCompletelyCommand = nil then
  begin
    FZIPFileHasBeenCreatedCompletelyCommand := FDBXConnection.CreateCommand;
    FZIPFileHasBeenCreatedCompletelyCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FZIPFileHasBeenCreatedCompletelyCommand.Text := 'TsvmDownloadMethods.ZIPFileHasBeenCreatedCompletely';
    FZIPFileHasBeenCreatedCompletelyCommand.Prepare;
  end;
  FZIPFileHasBeenCreatedCompletelyCommand.Parameters[0].Value.SetInt32(ZIPFileID);
  FZIPFileHasBeenCreatedCompletelyCommand.ExecuteUpdate;
  Result := FZIPFileHasBeenCreatedCompletelyCommand.Parameters[1].Value.GetBoolean;
end;

procedure TsvmDownloadMethodsClient.FetchZIPFile(ZIPFileID: Integer; Position: Int64; out Buffer: TStream; out BufferSize: Int64);
begin
  if FFetchZIPFileCommand = nil then
  begin
    FFetchZIPFileCommand := FDBXConnection.CreateCommand;
    FFetchZIPFileCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FFetchZIPFileCommand.Text := 'TsvmDownloadMethods.FetchZIPFile';
    FFetchZIPFileCommand.Prepare;
  end;
  FFetchZIPFileCommand.Parameters[0].Value.SetInt32(ZIPFileID);
  FFetchZIPFileCommand.Parameters[1].Value.SetInt64(Position);
  FFetchZIPFileCommand.ExecuteUpdate;
  Buffer := FFetchZIPFileCommand.Parameters[2].Value.GetStream(FInstanceOwner);
  BufferSize := FFetchZIPFileCommand.Parameters[3].Value.GetInt64;
end;

constructor TsvmDownloadMethodsClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;

constructor TsvmDownloadMethodsClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;

destructor TsvmDownloadMethodsClient.Destroy;
begin
  FCompressFilesCommand.DisposeOf;
  FZIPFileHasBeenCreatedCompletelyCommand.DisposeOf;
  FFetchZIPFileCommand.DisposeOf;
  inherited;
end;

end.
