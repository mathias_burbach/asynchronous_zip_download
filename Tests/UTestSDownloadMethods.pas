unit UTestSDownloadMethods;

interface

uses
  UTestBase,
  SDownloadMethods;

type
  TTestsvmDownloadMethods = class(TBaseTestCase)
  private
    svmDownloadMethods: TsvmDownloadMethods;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestCompressFiles;
  end;

implementation

uses
  System.SysUtils,
  System.Classes,
  Vcl.Forms,
  UCommon,
  DTestHelper;

{ TTestsvmDownloadMethods }

procedure TTestsvmDownloadMethods.SetUp;
begin
  inherited;
  svmDownloadMethods := TsvmDownloadMethods.Create(nil);
end;

procedure TTestsvmDownloadMethods.TearDown;
begin
  svmDownloadMethods.Free;
  inherited;
end;

procedure TTestsvmDownloadMethods.TestCompressFiles;
const
  cFileName = 'Fetched.zip';
var
  ZIPFileID: Integer;
  fsFetchedZIPFile: TFileStream;
  Position: Int64;
  Buffer: TStream;
  BufferSize: Int64;
  DBValue: String;
begin
  RunCreateFixtureAtTearDown := True;
  svmDownloadMethods.CompressFiles(ZIPFileID);
  while not svmDownloadMethods.ZIPFileHasBeenCreatedCompletely(ZIPFileID) do
  begin
    Sleep(50);
    Application.ProcessMessages;
  end;
  try
    fsFetchedZIPFile := TFileStream.Create(cFileName, fmCreate);
    try
      Position := 0;
      repeat
        svmDownloadMethods.FetchZIPFile(ZIPFileID, Position, Buffer, BufferSize);
        Buffer.Position := 0;
        fsFetchedZIPFile.CopyFrom(Buffer, BufferSize);
        Position := Position + BufferSize;
      until (BufferSize <= cZIPFileChunk);
    finally
      fsFetchedZIPFile.Free;
      Buffer.Free; // normally done by DataSnap
    end;
    DBValue := dmoTestHelper.GetDBValue('ZIPFile', 'Count(1)', 'DeletedStmp Is Not Null');
    CheckEquals('1', DBValue, 'Deleted Count');
  finally
    if FileExists(cFileName) then
      DeleteFile(cFileName);
  end;
end;

end.

end.
