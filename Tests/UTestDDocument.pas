unit UTestDDocument;

interface

uses
  Datasnap.DBClient,
  UTestBase,
  DDocument;

type
  TTestdmoDocument = class(TBaseTestCase)
  private
    dmoDocument: TdmoDocument;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestCompressFiles;
  end;

implementation

uses
  System.SysUtils,
  System.Classes,
  System.IOUtils,
  UCommon,
  DTestHelper;

{ TTestsvmDownloadMethods }

procedure TTestdmoDocument.SetUp;
begin
  inherited;
  dmoDocument := TdmoDocument.Create(nil);
end;

procedure TTestdmoDocument.TearDown;
begin
  dmoDocument.Free;
  inherited;
end;

procedure TTestdmoDocument.TestCompressFiles;
const
  cZIPFileID = 1;
  cFileSize = 123506;
var
  FileName: string;
begin
  FileName := IncludeTrailingPathDelimiter(TPath.GetTempPath) + cZIPFileID.ToString + '.zip';
  try
    dmoDocument.CompressFiles(cZIPFileID);
    CheckEquals(cFileSize, GetFileSize(FileName), 'FileSize');
  finally
    if FileExists(FileName) then
      DeleteFile(FileName);
  end;
end;

end.
