unit UTestSetup;

interface

uses
  TestFrameWork,
  TestExtensions;

type
  TDownloadTestSetup = class(TTestSetup)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  end;

implementation

{ TDownloadTestSetup }

uses
  DTestHelper,
  UTestDDocument,
  UTestSDownloadMethods,
  UTestDMain;

procedure TDownloadTestSetup.SetUp;
begin
  inherited;
  dmoTestHelper := TdmoTestHelper.Create(nil);
  dmoTestHelper.CreateFixture;
end;

procedure TDownloadTestSetup.TearDown;
begin
  dmoTestHelper.Free;
  inherited;
end;

initialization
  RegisterTest(TDownloadTestSetup.Create(
                 TTestSuite.Create('All Tests',
                                   [TTestSuite.Create('Server Tests',
                                                      [TTestdmoDocument.Suite,
                                                       TTestsvmDownloadMethods.Suite]),
                                    TTestSuite.Create('Client Tests',
                                                      [TTestdmoMain.Suite])])));

end.
