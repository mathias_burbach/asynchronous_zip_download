program DownloadTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  UCommon in '..\Common\UCommon.pas',
  DDocument in '..\Server\DDocument.pas' {dmoDocument: TDataModule},
  UTestBase in 'UTestBase.pas',
  DTestHelper in 'DTestHelper.pas' {dmoTestHelper: TDataModule},
  UTestDDocument in 'UTestDDocument.pas',
  UTestSetup in 'UTestSetup.pas',
  SDownloadMethods in '..\Server\SDownloadMethods.pas' {svmDownloadMethods: TDataModule},
  UCompressWorkerThread in '..\Server\UCompressWorkerThread.pas',
  UCompressWorkerThreadList in '..\Server\UCompressWorkerThreadList.pas',
  UTestSDownloadMethods in 'UTestSDownloadMethods.pas',
  DMain in '..\Client\DMain.pas' {dmoMain: TDataModule},
  UTestDMain in 'UTestDMain.pas',
  UServerProxy in '..\Client\UServerProxy.pas';

{$R *.RES}

begin
  ReportMemoryLeaksOnShutdown := True;
  DUnitTestRunner.RunRegisteredTests;
end.

