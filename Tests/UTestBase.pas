unit UTestBase;

interface

uses
  TestFramework;

type
  TBaseTestCase = class(TTestCase)
  private
    FRunCreateFixtureAtTearDown: Boolean;
    procedure SetRunCreateFixtureAtTearDown(const Value: Boolean);
  protected
    procedure SetUp; override;
    procedure TearDown; override;
    function GetFileSize(const FileName: string): Longint;
  public
    property RunCreateFixtureAtTearDown: Boolean read FRunCreateFixtureAtTearDown write SetRunCreateFixtureAtTearDown;
  end;

implementation

{ TAIMSABaseTestCase }

uses
  DTestHelper,
  UCommon;

{ TAppServerBaseTestCase }

function TBaseTestCase.GetFileSize(const FileName: string): Longint;
begin
  Result := UCommon.GetFileSize(FileName);
end;

procedure TBaseTestCase.SetRunCreateFixtureAtTearDown(const Value: Boolean);
begin
  FRunCreateFixtureAtTearDown := Value;
end;

procedure TBaseTestCase.SetUp;
begin
  inherited;
  FRunCreateFixtureAtTearDown := False;
end;

procedure TBaseTestCase.TearDown;
begin
  if FRunCreateFixtureAtTearDown then
  begin
    dmoTestHelper.CreateFixture;
    FRunCreateFixtureAtTearDown := False;
  end;
  inherited;
end;

end.
