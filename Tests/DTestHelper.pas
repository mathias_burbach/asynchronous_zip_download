unit DTestHelper;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Comp.ScriptCommands, FireDAC.Stan.Util,
  FireDAC.Comp.Script, FireDAC.Comp.UI;

type
  TdmoTestHelper = class(TDataModule)
    conDownloadDB: TFDConnection;
    qryGetDBValue: TFDQuery;
    scrCreateFixture: TFDScript;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateFixture;
    function GetDBValue(const TableName, ColumnName, WhereClause: string): string;
  end;

var
  dmoTestHelper: TdmoTestHelper;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoTestHelper }

procedure TdmoTestHelper.CreateFixture;
begin
  conDownloadDB.StartTransaction;
  try
    scrCreateFixture.ExecuteAll;
    conDownloadDB.Commit;
  except
    conDownloadDB.Rollback;
    raise;
  end;
end;

procedure TdmoTestHelper.DataModuleCreate(Sender: TObject);
begin
  conDownloadDB.Open;
end;

procedure TdmoTestHelper.DataModuleDestroy(Sender: TObject);
begin
  conDownloadDB.Close;
end;

function TdmoTestHelper.GetDBValue(const TableName, ColumnName, WhereClause: string): string;
begin
  Result := '[Not Found]';
  qryGetDBValue.SQL.Clear;
  qryGetDBValue.SQL.Add('Select ' + ColumnName);
  qryGetDBValue.SQL.Add('From ' + TableName);
  if WhereClause <> EmptyStr then
    qryGetDBValue.SQL.Add('Where ' + WhereClause);
  qryGetDBValue.Open;
  try
    if not qryGetDBValue.Eof then
      Result := qryGetDBValue.Fields[0].AsString;
  finally
    qryGetDBValue.Close;
  end;
end;

end.
