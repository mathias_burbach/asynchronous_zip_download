object dmoTestHelper: TdmoTestHelper
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 163
  Width = 339
  object conDownloadDB: TFDConnection
    Params.Strings = (
      'Database=DownloadDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Protocol=TCPIP'
      'Server=127.0.0.1'
      'DriverID=FB')
    FetchOptions.AssignedValues = [evUnidirectional]
    FetchOptions.Unidirectional = True
    LoginPrompt = False
    Left = 48
    Top = 40
  end
  object qryGetDBValue: TFDQuery
    Connection = conDownloadDB
    Left = 248
    Top = 40
  end
  object scrCreateFixture: TFDScript
    SQLScripts = <
      item
        Name = 'AllInOne'
        SQL.Strings = (
          'Delete From ZIPFile;'
          'Alter Sequence DownloadDBSeq Restart With 0;')
      end>
    Connection = conDownloadDB
    Params = <>
    Macros = <>
    FetchOptions.AssignedValues = [evItems, evAutoClose, evAutoFetchAll]
    FetchOptions.AutoClose = False
    FetchOptions.Items = [fiBlobs, fiDetails]
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand, rvDirectExecute, rvPersistent]
    ResourceOptions.MacroCreate = False
    ResourceOptions.DirectExecute = True
    Left = 144
    Top = 40
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 48
    Top = 104
  end
end
