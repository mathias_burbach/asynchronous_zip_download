unit UTestDMain;

interface

uses
  DMain,
  UTestBase;

type
  TTestdmoMain = class(TBaseTestCase)
  private
    dmoMain: TdmoMain;
    FCompleted: Boolean;
    procedure DoDownloadCompleted(Sender: TObject);
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestDownloadCompressedFiles;
  end;


implementation

uses
  System.SysUtils,
  Vcl.Forms;

{ TTestdmoMain }

procedure TTestdmoMain.DoDownloadCompleted(Sender: TObject);
begin
  FCompleted := True;
end;

procedure TTestdmoMain.SetUp;
begin
  inherited;
  dmoMain := TdmoMain.Create(nil);
  dmoMain.OnDownloadCompleted := DoDownloadCompleted;
end;

procedure TTestdmoMain.TearDown;
begin
  dmoMain.Free;
  inherited;
end;

procedure TTestdmoMain.TestDownloadCompressedFiles;
const
  cFileName = 'Fetched.zip';
begin
  try
    FCompleted := False;
    dmoMain.DownloadCompressedFiles(cFileName);
    while not FCompleted do
    begin
      Sleep(50);
      Application.ProcessMessages;
    end;
  finally
    if FileExists(cFileName) then
      DeleteFile(cFileName);
  end;
end;

end.
